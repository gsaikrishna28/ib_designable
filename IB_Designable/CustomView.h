//
//  CustomView.h
//  IB_Designable
//
//  Created by Krishna Gunda on 7/21/16.
//  Copyright © 2016 Krishna Gunda. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface CustomView : UIView

@property(assign, nonatomic) IBInspectable CGFloat borderWidth;
@property(strong, nonatomic) IBInspectable UIColor *borderColor;
@property(strong, nonatomic) IBInspectable UIColor *progressColor;
@property(assign, nonatomic) IBInspectable CGFloat progress;
@property (strong,nonatomic) UILabel *progressLBL;
@property (strong,nonatomic) NSString *progressLBLText;


@end
