//
//  CustomView.m
//  IB_Designable
//
//  Created by Krishna Gunda on 7/21/16.
//  Copyright © 2016 Krishna Gunda. All rights reserved.
//

#import "CustomView.h"

@interface CustomView()

@property (strong, nonatomic) CAShapeLayer *backLayer;
@property (strong, nonatomic) CAShapeLayer *frontLayer;

@end

@implementation CustomView
@synthesize backLayer, frontLayer;

- (instancetype)initWithFrame:(CGRect)frame
{
    self.progressLBL = [[UILabel alloc] init];
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaults];
        [self configureLabel];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self.progressLBL = [[UILabel alloc] init];
    self = [super initWithCoder:coder];
    if (self) {
        [self setupDefaults];
        [self configureLabel];
    }
    return self;
}

-(void)prepareForInterfaceBuilder{
    [super prepareForInterfaceBuilder];
    [self configureLabel];
}

-(void) setupDefaults {
    self.borderWidth = 5;
    self.borderColor = [UIColor grayColor];
    self.borderColor = [UIColor greenColor];
    self.progress = 0.6;
}

-(void) configureLabel {
    self.progressLBL = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth(self.frame), 60.0)];
    
    self.progressLBL.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:60];
    self.progressLBL.textColor = [UIColor blackColor];
    self.progressLBL.textAlignment = NSTextAlignmentCenter;
    self.progressLBL.text = @"0";
    self.progressLBL.translatesAutoresizingMaskIntoConstraints = false;
    [self addSubview:self.progressLBL];
    
    
    NSLayoutConstraint *captionLabelCenterX = [self.progressLBL.centerXAnchor constraintEqualToAnchor:self.centerXAnchor];
    NSLayoutConstraint *captionLabelBottom = [self.progressLBL.centerYAnchor constraintEqualToAnchor:self.centerYAnchor];
    
    [NSLayoutConstraint activateConstraints:[NSArray arrayWithObjects:captionLabelCenterX,captionLabelBottom, nil]];
}

-(void) layoutSubviews {
    [super layoutSubviews];
    [self updateLayerProperties];
}

-(void) setBorderWidth:(CGFloat)borderWidth {
    if (borderWidth != _borderWidth) {
        _borderWidth = borderWidth;
        [self updateLayerProperties];
    }
}

-(void) setBorderColor:(UIColor *)borderColor {
    if (_borderColor != borderColor) {
        _borderColor = borderColor;
        [self updateLayerProperties];
    }
}

-(void) setProgress:(CGFloat)progress {
    if (progress != _progress) {
        _progress = progress;
        [self updateLayerProperties];
    }
}

-(void) setProgressColor:(UIColor *)progressColor {
    if (_progressColor != progressColor) {
        _progressColor = progressColor;
        [self updateLayerProperties];
    }
}

-(void) setProgressLBLText:(NSString *)progressLBLText {
    self.progressLBL.text = progressLBLText;
    [self updateLayerProperties];
}

-(void) updateLayerProperties {
    
    if (self.backLayer == nil) {
        self.backLayer = [CAShapeLayer layer];
        [self.layer addSublayer:self.backLayer];
    }
    
    CGRect backRect = CGRectInset(self.bounds, _borderWidth, _borderWidth);
    self.backLayer.path = [UIBezierPath bezierPathWithOvalInRect:backRect].CGPath;
    self.backLayer.fillColor = [UIColor clearColor].CGColor;
    self.backLayer.lineWidth = _borderWidth;
    self.backLayer.strokeColor = _borderColor.CGColor;
    
    if (self.frontLayer == nil) {
        self.frontLayer = [CAShapeLayer layer];
        self.frontLayer.transform = CATransform3DRotate(self.frontLayer.transform, -M_PI/2, 0, 0, 1);
        [self.layer addSublayer:self.frontLayer];
    }
    CGRect frontRect = CGRectInset(self.bounds, _borderWidth, _borderWidth);
    self.frontLayer.path = [UIBezierPath bezierPathWithOvalInRect:frontRect].CGPath;
    self.frontLayer.fillColor = [UIColor clearColor].CGColor;
    self.frontLayer.lineWidth = _borderWidth;
    self.frontLayer.strokeColor = _progressColor.CGColor;
    self.frontLayer.strokeEnd = self.progress;
    
    self.backLayer.frame = self.bounds;
    self.frontLayer.frame = self.bounds;
}

@end
