//
//  ViewController.m
//  IB_Designable
//
//  Created by Krishna Gunda on 7/21/16.
//  Copyright © 2016 Krishna Gunda. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize progressCustomView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.progressCustomView setProgressLBLText:@"Loading"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
