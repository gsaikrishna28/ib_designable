//
//  ViewController.h
//  IB_Designable
//
//  Created by Krishna Gunda on 7/21/16.
//  Copyright © 2016 Krishna Gunda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet CustomView *progressCustomView;

@end

