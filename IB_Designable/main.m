//
//  main.m
//  IB_Designable
//
//  Created by Krishna Gunda on 7/21/16.
//  Copyright © 2016 Krishna Gunda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
